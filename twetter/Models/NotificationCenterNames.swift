//
//  NotificationCenterNames.swift
//  twetter
//
//  Created by Nicholas Chung on 1/31/17.
//  Copyright © 2017 Nicholas Chung. All rights reserved.
//
import Foundation

enum UserNotificationCenterOps: String {
    case userDidLogout = "UserDidLogOut"
    
    var notification: Notification.Name {
        return Notification.Name(rawValue: self.rawValue)
    }
}
