//
//  TweetEntities.swift
//  twetter
//
//  Created by Nicholas Chung on 2/2/17.
//  Copyright © 2017 Nicholas Chung. All rights reserved.
//

import UIKit

class TweetEntities: NSObject {
    
    // Holds info about the width/height and url of the medias
    var mediaInfo: [TweetMedia]?
    
    init(entitiesDictionary: NSDictionary) {
        if let mediaDictionary: [NSDictionary] = entitiesDictionary["media"] as? [NSDictionary] {
            self.mediaInfo = mediaDictionary.map({ (mediaDictionary) -> TweetMedia in
                TweetMedia(mediaDictionary: mediaDictionary)
            })
        }
    }
}
